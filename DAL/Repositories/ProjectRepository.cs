﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using WebAPI.Proj.DAL.Entities;
using WebServer.Repositories.Interface;
using WebAPI.Proj.DAL.Context;


namespace WebAPI.Proj.DAL.Repositories
{
    public class ProjectRepository : IRepository<Project> 
    {
        static string JsonString = File.ReadAllText($@"{Directory.GetCurrentDirectory()}\Data\Projects.json");
        public static List<Project> Entities { get; set; } = JsonConvert.DeserializeObject<List<Project>>(JsonString);


        public void Create(Project project)
        {
            Entities.Add(project);
        }
        
        public IEnumerable<Project> Read()
        {
            return Entities;
        }
        
        public void Update(int id, Project new_project)
        {
            int idNode = Entities.IndexOf(Entities.First(e => e.Id == id));
            Entities[idNode] = new_project;
        }

        public void Delete(int id)
        {
            var e = Entities.First(e => e.Id == id);
            Entities.Remove(e);
        }
    }
}
