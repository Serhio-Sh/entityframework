﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using WebAPI.Proj.DAL.Entities;
using WebServer.Repositories.Interface;

namespace WebAPI.Proj.DAL.Repositories
{
    public class TeamRepository : IRepository<Team> 
    {
        static string JsonString = File.ReadAllText($@"{Directory.GetCurrentDirectory()}\Data\Teams.json");
        static List<Team> Entities { get; set; } = JsonConvert.DeserializeObject<List<Team>>(JsonString);


        public void Create(Team team)
        {
            Entities.Add(team);
        }

        public IEnumerable<Team> Read()
        {
            return Entities;
        }

        public void Update(int id, Team new_team)
        {
            Entities[id] = new_team;
        }

        public void Delete(int id)
        {
            Entities = Entities.Where(x => x.Id != id).ToList();
        }
    }
}
