﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebAPI.Proj.DAL.Context;
using WebAPI.Proj.DAL.Entities;

namespace WebAPI.Proj.BLL
{
    public class TasksService : BaseService
    {
        public TasksService(WebDbContext context) : base(context)
        {
        }

        public async void Create(Task task)
        {
            _context.Tasks.Add(task);
            await _context.SaveChangesAsync();
        }

        public IEnumerable<Task> Read()
        {
            return _context.Tasks.ToList(); ;
        }

        public async void Update(Task new_task)
        {
            _context.Tasks.Update(new_task);
            await _context.SaveChangesAsync();
        }

        public async void Delete(int id)
        {
            var task = _context.Tasks.Where(p => p.Id == id).First();
            _context.Tasks.Remove(task);

            await _context.SaveChangesAsync();
        }
    }
}
