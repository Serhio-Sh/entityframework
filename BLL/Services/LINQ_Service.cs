﻿using System;
using System.Linq;
using System.Collections.Generic;
using WebAPI.Proj.Common.DTO;
using WebAPI.Proj.DAL.Repositories;
using WebAPI.Proj.DAL.Entities;
using WebAPI.Proj.DAL.Context;

namespace WebAPI.Proj.BLL
{
    public class LINQ_Serviсe : BaseService
    {
        public LINQ_Serviсe(WebDbContext context) 
            : base (context)
        {
        }

        //Получить кол-во тасков у проекта конкретного пользователя (по id) (словарь, где ключом будет проект, а значением кол-во тасков).
        public Dictionary<string, int> GetNumberTasks(int id)
        {            
                var c = _context.Tasks.GroupBy(task => task.ProjectId)
                .Join(_context.Users,
                tasks => tasks.Key, 
                user => user.Id,
                (tasks, user) => new 
                {
                    Project = tasks.Key,
                    NumbTasks = tasks.Count()
                });
             
            return c.ToDictionary(x => x.Project.ToString(), x => x.NumbTasks);
        }

        //Получить список тасков, назначенных на конкретного пользователя (по id), где name таска < 45 символов (коллекция из тасков).
        public List<TaskDTO> GetTasks(int id)
        {
            return _context.Tasks
                .Where(task => task.PerformerId == id && task.Name.Length < 45)
                .Select(task => new TaskDTO().FromTask(task))
                .ToList();
        }

        //Получить список (id, name) из коллекции тасков, которые выполнены (finished) в текущем (2021) году для конкретного пользова
        public List<(int, string)> GetFinished21(int id)
        {
            return _context.Tasks
                .Where(task => task.PerformerId == id && task.FinishedAt.Year == 2021)
                .ToList()
                .Select(task => (task.Id, task.Name))
                .ToList();
        }

        //Получить список (id, имя команды и список пользователей) из коллекции команд, участники которых старше 10 лет, отсортированных по дате
        //регистрации пользователя по убыванию, а также сгруппированных по командам.
        public List<(int, string, List<User>)> GetTeam_Users()
        {
            return _context.Users
                .GroupBy(user => user.TeamId)
                .Join(_context.Tasks,
                users => users.Key,
                team => team.Id,
                (users, team) => new
                {
                    team.Id,
                    team.Name,
                    users
                })
                .Where(node => node.users.All(user => user.BirthDay.Year < 2011)).ToList()
                .Select(team => (team.Id, team.Name, team.users.ToList())).ToList();
        }

        //Получить список пользователей по алфавиту first_name (по возрастанию) с отсортированными tasks по длине name (по убыванию).
        public Dictionary<string, List<TaskDTO>> GetUser_Tasks()
        {
            var v = from t in _context.Tasks.Select(t => new TaskDTO().FromTask(t))
                    orderby t.Name.Length descending
                    group t by t.PerformerId into tasks 
                    join u in _context.Users on tasks.Key equals u.Id
                    orderby u.FirstName
                    select new
                    {
                        UserFirstName = u.FirstName,
                        Tasks = tasks.ToList()
                    };

            return v.ToDictionary(x => x.UserFirstName, x => x.Tasks);
        }
    }
}
