﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Proj.DAL.Context;
using WebAPI.Proj.DAL.Entities;

namespace WebAPI.Proj.BLL
{
    public class ProjectsService : BaseService
    {
        public ProjectsService(WebDbContext context) : base(context)
        {
        }

        public async void Create(Project project)
        {
            _context.Projects.Add(project);
            await _context.SaveChangesAsync();
        }

        public IEnumerable<Project> Read()
        {
           return _context.Projects.ToList(); ;
        }

        public async void Update(Project new_project)
        {
            _context.Projects.Update(new_project);
            await _context.SaveChangesAsync();
        }

        public async void Delete(int id)
        {
            var project = _context.Projects.Where(p => p.Id == id).First();
            _context.Projects.Remove(project);

            await _context.SaveChangesAsync();
        }
    }
}
