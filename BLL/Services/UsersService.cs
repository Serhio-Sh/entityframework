﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebAPI.Proj.DAL.Context;
using WebAPI.Proj.DAL.Entities;

namespace WebAPI.Proj.BLL
{
    public class UsersService : BaseService
    {
        public UsersService(WebDbContext context) : base(context)
        {
        }

        public async void Create(User team)
        {
            _context.Users.Add(team);
            await _context.SaveChangesAsync();
        }

        public IEnumerable<User> Read()
        {
            return _context.Users.ToList(); ;
        }

        public async void Update(User new_user)
        {
            _context.Users.Update(new_user);
            await _context.SaveChangesAsync();
        }

        public async void Delete(int id)
        {
            var team = _context.Users.Where(p => p.Id == id).First();
            _context.Users.Remove(team);

            await _context.SaveChangesAsync();
        }
    }
}
