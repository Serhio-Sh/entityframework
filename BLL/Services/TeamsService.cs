﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebAPI.Proj.DAL.Context;
using WebAPI.Proj.DAL.Entities;

namespace WebAPI.Proj.BLL
{
    public class TeamsService : BaseService
    {
        public TeamsService(WebDbContext context) : base(context)
        {
        }

        public async void Create(Team team)
        {
            _context.Teams.Add(team);
            await _context.SaveChangesAsync();
        }

        public IEnumerable<Team> Read()
        {
            return _context.Teams.ToList(); ;
        }

        public async void Update(Team new_team)
        {
            _context.Teams.Update(new_team);
            await _context.SaveChangesAsync();
        }

        public async void Delete(int id)
        {
            var team = _context.Teams.Where(p => p.Id == id).First();
            _context.Teams.Remove(team);

            await _context.SaveChangesAsync();
        }
    }
}
