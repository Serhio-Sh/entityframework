﻿using Newtonsoft.Json;
using System.Collections.Generic;
using WebAPI.Proj.DAL.Entities;

namespace WebAPI.Proj.Common.DTO
{
    public class ProjectDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Deadline { get; set; }
        public string CreatedAt { get; set; }

        public IEnumerable<TaskDTO> Tasks { get; set; }
        public User Autor { get; set; }
        public Team Team { get; set; }

    }
}
