﻿using System;
using Newtonsoft.Json;
using WebAPI.Proj.DAL.Enums;
using WebAPI.Proj.DAL.Entities;
using System.ComponentModel.DataAnnotations;

namespace WebAPI.Proj.Common.DTO
{
    public class TaskDTO
    {
        [Required]
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime FinishedAt { get; set; }

        public TaskDTO FromTask(Task task)
        {
            return new TaskDTO
            {
                Id = task.Id,
                ProjectId = task.ProjectId,
                Name = task.Name,
                Description = task.Description,
                State = task.State,
                CreatedAt = task.CreatedAt,
                FinishedAt = task.FinishedAt
            };
        }
    }
}
