﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using WebAPI.Proj.DAL.Entities;
using WebAPI.Proj.BLL;
using WebAPI.Proj.Common.DTO;
using Newtonsoft.Json;

namespace WebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetPluralController : ControllerBase
    {
        private LINQ_Serviсe LinqServiсe;
        public GetPluralController(LINQ_Serviсe serviсe)
        {
            LinqServiсe = serviсe;
        }

        [HttpGet("NumberTasks/{id}")]
        public ActionResult<string> GetNumberTasks(int id)
        {
            return Ok(LinqServiсe.GetNumberTasks(id));
        }

        [HttpGet("GetTasks/{id}")]
        public ActionResult<ICollection<TaskDTO>> GetTasks(int id)
        {
            return Ok(LinqServiсe.GetTasks(id));
        }

        [HttpGet("GetFinished21/{id}")]
        public ActionResult<ICollection<(int, string)>> GetFinished21(int id)
        {
            return Ok(LinqServiсe.GetFinished21(id));
        }

        [HttpGet("GetTeam_Users")]
        public ActionResult<ICollection<(int, string, List<User>)>> GetTeam_Users()
        {
            return Ok(LinqServiсe.GetTeam_Users());
        }


        [HttpGet("GetUser_Tasks")]
        public ActionResult<string> GetUser_Tasks()
        {
            return Ok(LinqServiсe.GetUser_Tasks());
        }

    }
}
