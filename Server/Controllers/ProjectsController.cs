﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WebAPI.Proj.DAL.Entities;
using WebAPI.Proj.BLL;


namespace WebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectsService _projectService;
        public ProjectsController(ProjectsService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public ActionResult<ICollection<Project>> Get()
        {
            return Ok(_projectService.Read());
        }

        [HttpPost]
        public void Post([FromBody] Project dto)
        {
            _projectService.Create(dto);
        }

        [HttpPut]
        public void Put([FromBody] Project project)
        {
            _projectService.Update(project);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _projectService.Delete(id);
        }
    }
}
