﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WebAPI.Proj.DAL.Entities;
using WebAPI.Proj.BLL;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UsersService _userService;
        public UsersController(UsersService usersService)
        {
            _userService = usersService;
        }

        [HttpGet]
        public ActionResult<ICollection<User>> Get()
        {
            return Ok(_userService.Read());
        }

        [HttpPost]
        public void Post([FromBody] User dto)
        {
            _userService.Create(dto);
        }

        [HttpPut("{id}")]
        public void Put([FromBody] User user)
        {
            _userService.Update(user);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _userService.Delete(id);
        }
    }
}
