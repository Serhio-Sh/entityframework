﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WebAPI.Proj.DAL.Entities;
using WebAPI.Proj.BLL;

namespace WebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly TasksService _tasksService;
        public TasksController(TasksService tasksService)
        {
            _tasksService = tasksService;
        }

        [HttpGet]
        public ActionResult<ICollection<Task>> Get()
        {
            return Ok(_tasksService.Read());
        }

        [HttpPost]
        public void Post([FromBody] Task dto)
        {
            _tasksService.Create(dto);
        }

        [HttpPut("{id}")]
        public void Put([FromBody] Task task)
        {
            _tasksService.Update(task);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _tasksService.Delete(id);
        }
    }
}
