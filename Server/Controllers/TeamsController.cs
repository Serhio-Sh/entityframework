﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WebAPI.Proj.DAL.Entities;
using WebAPI.Proj.BLL;

namespace WebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly TeamsService _teamsService;
        public TeamsController(TeamsService teamsService)
        {
            _teamsService = teamsService;
        }

        [HttpGet]
        public ActionResult<ICollection<Team>> Get()
        {
            return Ok(_teamsService.Read());
        }

        [HttpPost]
        public void Post([FromBody] Team dto)
        {
            _teamsService.Create(dto);
        }

        [HttpPut("{id}")]
        public void Put([FromBody] Team team)
        {
            _teamsService.Update(team);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _teamsService.Delete(id);
        }
    }
}
