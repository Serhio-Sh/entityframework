﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using WebAPI.Proj.DAL.Entities;
using WebServer.Repositories.Interface;

namespace WebServer.Repositories
{
    public class TaskRepository: IRepository<Task> 
    {
        static string JsonString = File.ReadAllText($@"{Directory.GetCurrentDirectory()}\Data\Tasks.json");//@"Projects.json"
        static List<Task> Entities { get; set; } = JsonConvert.DeserializeObject<List<Task>>(JsonString);


        public void Create(Task task)
        {
            Entities.Add(task);
        }

        public IEnumerable<Task> Read()
        {
            return Entities;
        }

        public void Update(int id, Task new_task)
        {
            Entities[id] = new_task;
        }

        public void Delete(int id)
        {
            Entities = Entities.Where(x => x.Id != id).ToList();
        }
    }
}
