﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using WebAPI.Proj.DAL.Entities;
using WebServer.Repositories.Interface;


namespace WebServer.Repositories
{
    public class ProgectsRepository : IRepository<Project> 
    {
        static string JsonString = File.ReadAllText($@"{Directory.GetCurrentDirectory()}\Data\Projects.json");
        static List<Project> Entities { get; set; } = JsonConvert.DeserializeObject<List<Project>>(JsonString);


        public void Create(Project project)
        {
            Entities.Add(project);
        }
        
        public IEnumerable<Project> Read()
        {
            return Entities;
        }
        
        public void Update(int id, Project new_project)
        {
            Entities[id] = new_project;
        }

        public void  Delete(int id)
        {
            Entities.RemoveAt(id);
        }
    }
}
