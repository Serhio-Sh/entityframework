﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;
using WebAPI.Proj.Common.DTO;
using WebAPI.Proj.DAL.Entities;

namespace Client
{
    class Program
    {
        private const string APP_PATH = "http://localhost:19847";
        static readonly HttpClient client = new();

        static void Main(string[] args)
        {
            var response = client.GetStringAsync(APP_PATH + "/api/Projects").Result;
            Console.WriteLine("Projects\n" + FormatJson(response));

            response = client.GetStringAsync(APP_PATH + "/api/Tasks").Result;
            Console.WriteLine("Tasks\n" + FormatJson(response));

            response = client.GetStringAsync(APP_PATH + "/api/Teams").Result;
            Console.WriteLine("Teams\n" + FormatJson(response));

            response = client.GetStringAsync(APP_PATH + "/api/Users").Result;
            Console.WriteLine("Users\n" + FormatJson(response));
        }

        private const string INDENT_STRING = "    ";
        static string FormatJson(string json)
        {
            int indentation = 0;
            int quoteCount = 0;
            var result =
                from ch in json
                let quotes = ch == '"' ? quoteCount++ : quoteCount
                let lineBreak = ch == ',' && quotes % 2 == 0 ? ch + Environment.NewLine + String.Concat(Enumerable.Repeat(INDENT_STRING, indentation)) : null
                let openChar = ch == '{' || ch == '[' ? ch + Environment.NewLine + String.Concat(Enumerable.Repeat(INDENT_STRING, ++indentation)) : ch.ToString()
                let closeChar = ch == '}' || ch == ']' ? Environment.NewLine + String.Concat(Enumerable.Repeat(INDENT_STRING, --indentation)) + ch : ch.ToString()
                select lineBreak == null
                            ? openChar.Length > 1
                                ? openChar
                                : closeChar
                            : lineBreak;

            return String.Concat(result);
        }
    }
}
